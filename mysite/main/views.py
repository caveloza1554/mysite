from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Articulo
from .models import Orden
from django.contrib import messages
from .forms import OrdenForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
import requests
from requests.auth import HTTPBasicAuth
from django.conf import settings
import base64
from datetime import datetime, timedelta
import secrets
import json


# Create your views here.
def homepage(request, user_id=0, articulo_id=0):
    if request.method == 'POST':
        valor_total = 0
        articulo = Articulo.objects.get(id_articulo=articulo_id)
        precio_articulo = articulo.precio_articulo
        valor_total = int(precio_articulo) * int(request.POST['cantidad'])
        form = OrdenForm(request.POST.copy())
        form.data['user'] = user_id
        form.data['articulo'] = articulo_id
        form.data['valor_total'] = valor_total

        # check whether it's valid:
        if form.is_valid():
            form.save()
            messages.success(request, "Agregado con exito")
    else:
        form = OrdenForm()

    return render(request, "main/inicio.html", {"articulos": Articulo.objects.all, 'form': form})


def carrito(request, user_id):
    ordenes = Orden.objects.filter(user_id=user_id)
    tipos_articulos = Articulo.objects.all()
    factura = {}
    total_pagar = 0
    for tp in tipos_articulos:
        total = 0
        cantidad = 0
        ''' print(tp.id_articulo) '''
        for orden in ordenes:
            producto = Articulo.objects.get(id_articulo=orden.articulo_id)

            if tp.id_articulo == orden.articulo_id:
                factura[f'{producto.nombre_articulo}_{producto.id_articulo}'] = []
                total += int(orden.valor_total)
                cantidad += int(orden.cantidad)
                factura[f'{producto.nombre_articulo}_{producto.id_articulo}'].append({'url_imagen': producto.url_imagen, 'id_articulo': producto.id_articulo,
                                                                                      'total': total, 'cantidad': cantidad, 'nombre_articulo': producto.nombre_articulo})
        total_pagar += total

    return render(request, "main/carrito.html", {"factura": factura, "total_pagar": total_pagar})


def registro(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            usuario = form.save()
            nombre_usuario = form.cleaned_data.get('username')
            login(request, usuario)
            messages.success(
                request, f"{nombre_usuario} te has registrado con exito.")
            return redirect('main:homepage_get')
        else:
            for msg in form.error_messages:
                messages.error(request, f"{msg} {form.error_messages}")

    else:
        form = UserCreationForm()

    return render(request, "main/registro.html", {'form': form})


def ingreso(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            pdw = form.cleaned_data.get('password')
            user = authenticate(username=username, password=pdw)
            if user is not None:
                login(request, user)
                messages.info(request, f'Bienvenido {username}.')
                return redirect('main:homepage_get')
            else:
                messages.error(request, 'Error de contraseña o usuario.')
        else:
            for msg in form.error_messages:
                messages.error(request, f"{msg} {form.error_messages}")
    else:
        form = AuthenticationForm()
    return render(request, 'main/ingreso.html', {'form': form})


def salir(request):
    logout(request)
    messages.info(request, 'Saliste exitosamente')
    return redirect('main:ingreso')


def cancelar(request):
    id_user = request.GET['id_user']
    Orden.objects.filter(user_id=id_user).delete()
    ordenes = Orden.objects.filter(user_id=id_user)
    messages.error(request, 'Carrito Borrado')
    return redirect('main:homepage_get')


def pagar(request):

    if request.method == 'POST':
        id_user = request.POST['id_user']
        total_pagar = request.POST['total_pagar']
        url_pago = peticion_pago(total_pagar)
        if not url_pago:
            messages.error(request, 'Upps no se puede crear el pago')
            return redirect('main:homepage_get')
        else:
            return redirect(url_pago)


def crear_b64():
    to_b64 = f'{settings.TPAGA_USER}:{settings.TPAGA_PASSWORD}'
    texto = base64.b64encode(bytes(to_b64, 'utf-8'))
    return str(texto)


def peticion_pago(total_pagar):
    url_post_payment = settings.URL_PAYMENT
    idempotency_token = secrets.token_hex(16)
    expires_at = datetime.now() + timedelta(days=1)
    expires_at = expires_at.isoformat()
    data = {
        "cost": total_pagar,
        "purchase_details_url": "https://example.com/compra/348820",
        "voucher_url": "https://example.com/comprobante/348820",
        "idempotency_token": idempotency_token,
        "order_id": "348820",
        "terminal_id": "sede_45",
        "purchase_description": "Compra en My Store",
        "purchase_items": [
            {
                "name": "Aceite de girasol",
                "value": "13.390"
            },
            {
                "name": "Arroz X 80g",
                "value": "4.190"
            }
        ],
        "user_ip_address": "61.1.224.56",
        "expires_at": expires_at
    }

    data = json.dumps(data)
    headers = {
        'Content-Type': 'application/json'
    }

    peticion = requests.post(url_post_payment, data=data, headers=headers, auth=HTTPBasicAuth(
        settings.TPAGA_USER, settings.TPAGA_PASSWORD))

    if peticion.status_code != 201:
        return None

    res = peticion.json()
    return res['tpaga_payment_url']

def pago_exitoso(request):
    return render(request, 'main/pago_exitoso.html')