from django import forms
from .models import Orden
from django.forms import ModelForm
from .models import Articulo
from django.contrib.auth.models import User


class OrdenForm(ModelForm):
    articulo = forms.ModelChoiceField(
        widget=forms.HiddenInput(), queryset=Articulo.objects.all())
    user = forms.ModelChoiceField(
        widget=forms.HiddenInput(), queryset=User.objects.all())
    cantidad = forms.IntegerField(label='cantidad')
    valor_total = forms.IntegerField(
        label='valor_total', widget=forms.HiddenInput())

    ''' articulo = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    user = forms.IntegerField(widget=forms.HiddenInput(), required=False) '''

    class Meta:
        model = Orden
        fields = ['articulo', 'cantidad', 'user', 'valor_total']
