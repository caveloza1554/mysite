from django.urls import path, include
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.ingreso),
    path('homepage',
         views.homepage, name='homepage_get'),
    path('homepage/<int:user_id>/<int:articulo_id>',
         views.homepage, name='homepage_post'),
    path('carrito/<int:user_id>',
         views.carrito, name='carrito'),
    path('registro', views.registro, name='registro'),
    path('ingreso', views.ingreso, name='ingreso'),
    path('salir', views.salir, name='salir'),
    path('pagar', views.pagar, name='pagar'),
    path('cancelar', views.cancelar, name='cancelar'),
    path('pago', views.pago_exitoso, name='pago_exitoso'),
]
