from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
# Create your models here.


class Articulo(models.Model):
    id_articulo = models.IntegerField(primary_key=True)
    nombre_articulo = models.CharField(max_length=200)
    precio_articulo = models.IntegerField()
    url_imagen = models.CharField(max_length=500)

    ''' def __str__(self):
        return self.nombre_articulo '''


class Orden(models.Model):
    id_orden = models.IntegerField(primary_key=True)
    articulo = models.ForeignKey(
        Articulo,
        on_delete=models.CASCADE,
    )
    cantidad = models.IntegerField()
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )

    valor_total = models.IntegerField()

    ''' def __str__(self):
        return self.id_orden '''
