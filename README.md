# My Store

_Integracion API Tpaga con aplicacion Django_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._




### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

Tener instalada la ultima version de Python e instalar los requirements especificados en el archivo requirements.txt

```
pip install -r requirements.txt
```

### Ejecución 🔧

_Para ejecutar el programa desde la raiz te desplazas a la carpeta mysite y luego debes ejecutar el programa de Python:_

```
cd mysite
```
```
python manage.py runserver 0.0.0.0:8000
```

_Para su uso_

_Para crear productos ADMIN_ 
user: admin
pwd: pwdAdmin

_Para usar la aplicacion USER o bien puede registrarse_
user: usuario1
pwd: SoyUnaSuperClave

_Para usar la aplicacion desde la web en el pc que esta funcionando como servidor ingrese a:_

```
http://localhost:8000
```


_Para usarl la aplicacion desde un dispositivo movil, es obligatorio estar conectado a la misma red del pc que esta funcionando como servidor como paso adicional debemos conocer la ip que tiene el servidor para ingresar de esa manera_

_Tomando como ejemplo que la pc que esta funcionando como servidor tiene la ip 192.168.0.3 queda de la siguiente manera:_

```
http://192.168.0.3:8000
```